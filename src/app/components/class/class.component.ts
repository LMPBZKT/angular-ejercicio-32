import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {

  loading: boolean = false
  constructor() { }

  ngOnInit(): void {
  }

  cargar(){
    this.loading = true;

    setTimeout(() =>{
      this.loading = false;
    }, 4000)
  }
}
